import os
import os.path

DATA_MAJOR_VERSION = 1
DATA_MINOR_VERSION = 1
DATA_PATCH_VERSION = 0
DATA_BUILD_INFO = 250

RAW_DATA_ROOT = os.path.join(
    os.environ["DATA_PATH"] if "DATA_PATH" in os.environ else "data",
    f"v{DATA_MAJOR_VERSION}.{DATA_MINOR_VERSION}.{DATA_PATCH_VERSION}",
)

BUILD_DATA_ROOT = os.path.join(
    os.environ["DATA_PATH"] if "DATA_PATH" in os.environ else "data",
    f"v{DATA_MAJOR_VERSION}.{DATA_MINOR_VERSION}.{DATA_PATCH_VERSION}+{DATA_BUILD_INFO}",
)

COMET_USER = "raniere-silva"
