Preprint: https://arxiv.org/abs/2204.13857

Preprint as R Markdown: https://gitlab.com/raniere-phd/article-ai-for-radiograph-classification/


# AI for Radiograph Classification

Deep Convolutional Neural Network for Racehorse Radiograph Classification

Experiments ara available at [Comet](https://www.comet.ml/raniere-silva/ai-for-radiograph-classification/).

## Setting Environment with Conda

Run

```
$ conda env create --file environment.yml
```

## Data

The data was provided by [The Hong Kong Jockey Club](https://www.hkjc.com/)
under a non-disclosure agreement
and
we can't redistribute the data.

## Customise user defined variables

Edit `config.py`.

## Machine Learning Experiment

Run

```
$ python main.py
```

Metrics will be save in [Comet](https://www.comet.ml/).

Run

```
$ python main.py --help
```

to check available arguments.

## Files and Folders

- `agent.py`

  Has the knowledge to handle the data and model for train, validation and test.
- `bin`

  Has some useful scripts.
- `cam`

  Stores the class activation map of the model.
- `config.py`

  Has some user defined variables.
- `data`

  Stores the data. It should be replace with a symbolic link to the data.
- `data.ipynb`

  Describes the data.
- `data-preprocessing.ipynb`

  Describes how to preprocess the data.
- `dataset.py`

  Has the knowledge to load the data and create batches.
- `environment.yml`

  Conda environment.
- `main.py`

  Runnable script. It stores some hyperparameters.
- `model.py`

  Has the implementation of the model using PyTorch.
- `report.ipynb`

  Describes the accuracy of the model.
  This file can be converted to presentation format using

  ```
  $ jupyter nbconvert --to slides report.ipynb
  ```
- `weights`

  Stores the weights of trained model.

## References

