"""
Agent

Includes all the functions that train, validate, and test the model.
"""
import logging
import os.path

import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from sklearn import metrics
import torch
from torch import nn
import torch.optim as optim

import monai.visualize

import dataset
import model

logger = logging.getLogger("pytorch.agent")

# Select hardcopy backends for matplotlib
# https://raspberrypi.stackexchange.com/a/72562
matplotlib.use("Agg")
# Prepare canvas as described in
# https://stackoverflow.com/a/8218887/1802726
#
# Reuse the axes should make this code faster
# https://stackoverflow.com/a/11688881/1802726
fig = plt.figure(frameon=False)
fig.set_size_inches(1, 1)
ax = plt.Axes(fig, [0.0, 0.0, 1.0, 1.0])
ax.set_axis_off()
fig.add_axes(ax)


class Agent:
    """
    This base class will contain the base functions to be overloaded by any agent you will implement.
    """

    def __init__(
        self,
        hyper_params,
        no_training=False,
        class_activation_map=False,
        comet_logger=None,
    ):
        self.epoch = 0
        self.hyper_params = hyper_params
        self.comet = comet_logger
        logger.debug("Creating model ...")
        self.model = model.net
        logger.debug("Creating model completed.")
        self.best_model_state_dict = None

        if class_activation_map:
            self.cam_generator = monai.visualize.CAM(
                nn_module=self.model, target_layers=self.model.cam_target_layer
            )
        else:
            self.cam_generator = None

        if hyper_params["state-dict"]:
            logger.debug(
                f"Loading weights and biases from {hyper_params['state-dict']} ..."
            )
            self.model.load_state_dict(torch.load(hyper_params["state-dict"]))
            logger.debug(
                f"Loading weights and biases from {hyper_params['state-dict']} completed."
            )

        logger.debug("Sending model to device ...")
        self.model.to(self.hyper_params["device"])
        logger.debug("Sending model to device completed.")

        self.criterion = nn.CrossEntropyLoss()

        self.optimizer = optim.SGD(
            self.model.parameters(), lr=self.hyper_params["learning_rate"], momentum=0.9
        )

        self.data = {
            "train": None,
            "validation": None,
            "test": None,
        }

        if not no_training:
            logger.debug("Creating data train loader ...")
            self.data["train"] = dataset.TrainDataLoader(
                self.hyper_params["batch_size"]
            )
            logger.debug("Creating data train loader completed.")
            logger.debug("Creating data validation loader ...")
            self.data["validation"] = dataset.ValDataLoader(
                self.hyper_params["batch_size"]
            )
            logger.debug("Creating data train validation completed.")

        logger.debug("Creating data test loader ...")
        self.data["test"] = dataset.TestDataLoader(self.hyper_params["batch_size"])
        logger.debug("Creating data test loader completed.")

    def train(self):
        """
        Main training loop
        :return:
        """
        last_epoch_loss = float("inf")
        patience_counter = 0
        best_accuracy = 0

        for epoch in range(self.hyper_params["num_epochs"]):
            self.epoch = epoch

            with self.comet.train():
                logger.info(f"Training (epoch #{self.epoch}) ...")

                self.model.train()

                epoch_train_metrics = self.one_epoch("train")

                logger.info(f"Training (epoch #{self.epoch}) completed.")

            with self.comet.validate():
                logger.info(f"Validation after epoch #{self.epoch} ...")

                epoch_validation_metrics = self.one_epoch("validation")

                logger.info(f"Validation after epoch #{self.epoch} completed.")

                if best_accuracy < epoch_validation_metrics["accuracy"]:
                    logger.info(f"Validation accuracy improved at epoch #{self.epoch}!")
                    best_accuracy = epoch_validation_metrics["accuracy"]
                    logger.debug("Saving model state dict ...")
                    self.best_model_state_dict = self.model.state_dict()
                    logger.debug("Saving model state dict completed.")

            # Test for earlier stop
            loss_improvement = epoch_train_metrics["loss"] - last_epoch_loss
            last_epoch_loss = epoch_train_metrics["loss"]

            if loss_improvement > self.hyper_params["min_delta"]:
                patience_counter += 1
            else:
                patience_counter = 0

            if patience_counter >= self.hyper_params["patience"]:
                logger.info(
                    "Stopping training earlier based on loss function improvement."
                )
                break

    def one_epoch(self, mode):
        """
        One epoch of training
        :return:
        """
        running_loss = 0
        y_true = []
        y_predicted = []
        y_probability_estimation = []

        for batch_ndx, sample in enumerate(self.data[mode]):
            logger.debug(f"Batch #{batch_ndx} ...")

            inputs, labels, image_paths = sample.values()
            inputs = inputs.to(self.hyper_params["device"], dtype=torch.float)
            # CrossEntropyLoss requires torch.int64
            labels = labels.to(self.hyper_params["device"], dtype=torch.int64)

            # zero the parameter gradients
            self.optimizer.zero_grad()

            # forward + backward + optimize
            raw_outputs = self.model(inputs)
            loss = self.criterion(raw_outputs, labels)
            loss.backward()
            if mode == "train":
                self.optimizer.step()

            # Metrics
            running_loss += float(loss.item())

            probability_estimation = torch.nn.functional.softmax(raw_outputs, dim=1)
            prediction = torch.argmax(probability_estimation, dim=1)

            y_true.extend(labels.cpu().type(torch.int))
            y_predicted.extend(prediction.cpu())
            y_probability_estimation.extend(probability_estimation.cpu())

            if mode == "test" and self.cam_generator is not None:
                logger.debug("Saving class activation map ...")
                cam_results = self.cam_generator(x=inputs)
                for radiograph_ndx in range(inputs.shape[0]):
                    try:
                        cpu_array = inputs[radiograph_ndx][0].cpu()
                        cam_array = cam_results[radiograph_ndx][0].cpu()
                        ax.imshow(cpu_array, cmap="gray")
                        ax.imshow(
                            cam_array,
                            cmap="plasma_r",
                            alpha=0.4,
                            interpolation="bilinear",
                        )

                        head, image_filename = os.path.split(
                            image_paths[radiograph_ndx]
                        )
                        head, view = os.path.split(head)
                        head, limb = os.path.split(head)
                        head, laterality = os.path.split(head)

                        cam_fig_path = os.path.join(
                            "cam",
                            self.comet.get_key(),
                            laterality,
                            limb,
                            view,
                            image_filename,
                        )

                        os.makedirs(os.path.dirname(cam_fig_path), exist_ok=True)
                        plt.savefig(cam_fig_path, dpi=250)
                    except:
                        logger.error(
                            f"Fail to create class activation map for {image_paths[radiograph_ndx]}"
                        )
                logger.debug("Saving class activation map completed.")

            logger.debug(f"Batch #{batch_ndx} completed.")

        epoch_loss = running_loss / len(self.data[mode])

        # Need to convert the list of tensors to tensor.
        y_probability_estimation = torch.vstack(y_probability_estimation)
        y_probability_estimation = y_probability_estimation.detach().numpy()

        if mode == "test":
            label_list = self.data[mode].le.classes_.tolist()

            logger.info("Saving raw test classification ...")
            raw_test_classification_path = os.path.join(
                "classification", f"{self.comet.get_key()}.csv"
            )
            os.makedirs(os.path.dirname(raw_test_classification_path), exist_ok=True)

            df = pd.DataFrame(
                {
                    "image": [
                        self.data[mode].index_to_example_function(i)
                        for i in range(len(y_true))
                    ],
                    "true": [label_list[i] for i in y_true],
                    "predicted": [label_list[i] for i in y_predicted],
                }
            )
            df.to_csv(raw_test_classification_path, index=False)
            self.comet.log_asset(
                raw_test_classification_path,
                "test-classification.csv",
                copy_to_tmp=False,
            )

            logger.info("Saving raw test classification completed.")

            logger.info("Saving confusion matrix ...")
            self.comet.log_confusion_matrix(
                y_true,
                y_predicted,
                labels=label_list,
                index_to_example_function=self.data[mode].index_to_example_function,
                max_categories=48,
            )
            logger.info("Saving confusion matrix completed.")

        logger.info("Saving metrics ...")
        epoch_metrics = {
            "loss": epoch_loss,
            "accuracy": metrics.accuracy_score(y_true, y_predicted),
            "ROC AUC": metrics.roc_auc_score(
                y_true, y_probability_estimation, average="macro", multi_class="ovo"
            ),
        }
        logger.info(f"{epoch_metrics}")
        self.comet.log_metrics(epoch_metrics, epoch=self.epoch)
        logger.info("Saving metrics completed.")

        return epoch_metrics

    def test(self):
        """
        Main test loop
        :return:
        """
        if self.best_model_state_dict is not None:
            logger.debug("Loading model with best accuracy ...")
            self.model.load_state_dict(self.best_model_state_dict)
            logger.debug("Loading model with best accuracy completed.")
        self.one_epoch("test")

    def finalize(self):
        """
        Finalizes all the operations
        :return:
        """
        pass

    def save(self):
        """
        Save weights
        :return:
        """
        filename = f"{self.comet.get_key()}.pth"
        weights_path = os.path.join("weights", filename)
        logger.info(f"Saving model state to {weights_path} ...")
        torch.save(
            self.best_model_state_dict
            if self.best_model_state_dict is not None
            else self.model.state_dict(),
            weights_path,
        )
        logger.info(f"Saving model state to {weights_path} completed.")
