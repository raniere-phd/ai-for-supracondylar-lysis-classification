default:
  tags:
    - python
    - cuda
    - pytorch
    - racehorse

stages:
  - check
  - train
  - store
  - report
  - publish

before_script:
  - echo $CONDA_PATH
  - echo $DATA_PATH
  - echo $PATH
  - export PATH="${CONDA_PATH}:${PATH}"
  - source activate racehorse
  - python --version  # For debugging
 
test:
  stage: check
  script:
    - black --check .
    - pyflakes *.py
  timeout: 30m

model-training:
  stage: train
  script:
    - python main.py --save-state-dict --verbose
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_LABELS =~ /model/
  artifacts:
    public: false
    expire_in: 1 week
    paths:
      - weights/*.pth
      - comet/*.zip
      - classification/*.csv

upload-to-comet:
  stage: store
  script:
    - export EXPERIMENT_ID=$(ls -t comet | head -n 1 | sed 's/.zip//')
    - comet upload comet/${EXPERIMENT_ID}.zip
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_LABELS =~ /model/

generate-report:
  stage: report
  script:
    - export EXPERIMENT_ID=$(ls -t comet | head -n 1 | sed 's/.zip//')
    - papermill --kernel python3 --no-progress-bar --parameters experiment_id ${EXPERIMENT_ID} report.ipynb report/${EXPERIMENT_ID}.ipynb
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_LABELS =~ /model/
  artifacts:
    public: false
    expire_in: 1 week
    paths:
      - report/*.ipynb

generate-html:
  stage: publish
  script:
    - export EXPERIMENT_ID=$(ls -t report | head -n 1 | sed 's/.ipynb//')
    - jupyter nbconvert -y --to=html --output-dir=report report/${EXPERIMENT_ID}.ipynb
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_LABELS =~ /model/
  artifacts:
    public: false
    expire_in: 1 week
    paths:
      - report/*.html