"""
Main

Manager the agent to train, validate, and test the model.
At the end of the operation,
a report is submited to Comet.
"""
import argparse
import logging

import comet_ml
import torch

from agent import Agent
from model import name as model_name
import config


hyper_params = {
    "device": "cuda" if torch.cuda.is_available() else "cpu",
    "num_epochs": None,
    "batch_size": None,
    "learning_rate": None,
    "min_delta": None,
    "patience": None,
    "state-dict": None,
}


def main(
    no_training=False,
    save_state_dict=False,
    class_activation_map=False,
    tags=None,
):
    """Main loop"""
    logger.info("Starting experiment ...")
    experiment = comet_ml.OfflineExperiment(
        project_name="ai-for-radiograph-classification",
        workspace=config.COMET_USER,
        log_code=True,
        log_git_metadata=True,
        log_git_patch=True,
        log_graph=False,
        auto_param_logging=False,
        auto_metric_logging=False,
        auto_output_logging=False,
        auto_log_co2=False,
        disabled=False,
        offline_directory="comet/",
    )
    logger.info(f"Experiment register under the ID {experiment.get_key()}")
    experiment.log_parameters(hyper_params)
    data_version = f"v{config.DATA_MAJOR_VERSION}.{config.DATA_MINOR_VERSION}.{config.DATA_PATCH_VERSION}+{config.DATA_BUILD_INFO}"
    if tags is None:
        tags = [
            data_version,
            model_name,
            "SGD",
        ]
    else:
        tags.extend(
            [
                data_version,
                model_name,
                "SGD",
            ]
        )
    experiment.add_tags(tags)

    logger.debug("Creating agent ...")
    agent = Agent(
        hyper_params,
        no_training=no_training,
        class_activation_map=class_activation_map,
        comet_logger=experiment,
    )
    logger.debug("Creating agent completed.")

    # Optimization Loop
    if not no_training:
        logger.info("Training model ...")
        agent.train()
        logger.info("Training model completed.")

        if save_state_dict:
            logger.info("Saving model ...")
            agent.save()
            logger.info("Saving model completed.")

    with experiment.test():
        logger.info("Testing model ...")
        agent.test()
        logger.info("Testing model completed.")

    logger.info("Finishing experiment ...")
    experiment.end()
    logger.info("Finishing experiment completed.")

    agent.finalize()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="AI for Radiograph Classification",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "--tag", action="append", type=str, help="Tag to be record by Comet"
    )

    hyper_params_parser = parser.add_argument_group("hyper params")
    hyper_params_parser.add_argument("--batch", type=int, default=32, help="Batch size")
    hyper_params_parser.add_argument(
        "--epochs", type=int, default=128, help="Number of epochs"
    )
    hyper_params_parser.add_argument(
        "--learning-rate", type=float, default=1e-3, help="Learning Rate"
    )
    hyper_params_parser.add_argument(
        "--min-delta",
        type=float,
        default=0.1,
        help="Minimum increase qualify as an improvement",
    )
    hyper_params_parser.add_argument(
        "--patience",
        type=int,
        default=3,
        help="Number of epochs without improvement before stop the training.",
    )

    transfer_learning_parser = parser.add_argument_group("transfer learning")
    transfer_learning_parser.add_argument(
        "--no-training", action="store_true", help="Skip training"
    )
    transfer_learning_parser.add_argument(
        "--state-dict",
        default=None,
        help="Filename with weights and biases to load into the model",
    )
    transfer_learning_parser.add_argument(
        "--save-state-dict", action="store_true", help="Save model state"
    )

    parser.add_argument(
        "--class-activation-map",
        action="store_true",
        help="Generate class activation map.",
    )

    log_level_parser = parser.add_argument_group("log level")
    log_level_parser.add_argument(
        "--debug", action="store_true", help="Set logging level to DEBUG"
    )
    log_level_parser.add_argument(
        "--verbose", action="store_true", help="Set logging level to INFO"
    )

    args = parser.parse_args()

    if args.no_training is True and args.state_dict is None:
        parser.error("--state-dict can NOT be None with --no-training")

    logging.basicConfig()
    logger = logging.getLogger("pytorch")
    if args.verbose:
        logger.setLevel(logging.INFO)
    if args.debug:
        logger.setLevel(logging.DEBUG)

    hyper_params["batch_size"] = args.batch
    hyper_params["num_epochs"] = args.epochs
    hyper_params["learning_rate"] = args.learning_rate
    hyper_params["min_delta"] = args.min_delta
    hyper_params["patience"] = args.patience
    hyper_params["state-dict"] = args.state_dict

    main(
        args.no_training,
        args.save_state_dict,
        args.class_activation_map,
        args.tag,
    )
