"""
Model

Include the definition of the model.
"""
import torch.nn as nn
from torchvision.models import resnet34

name = "ResNet-34"

net = resnet34(pretrained=False, progress=False)
net.conv1 = nn.Conv2d(1, 64, 7, stride=2, padding=3, bias=False)
# Fine tuning
# https://pytorch.org/tutorials/beginner/finetuning_torchvision_models_tutorial.html
net.fc = nn.Linear(512, 48)
net.cam_target_layer = "layer4"
